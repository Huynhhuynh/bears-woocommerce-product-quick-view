# bears-woocommerce-product-quick-view
WooCommerce add on product quickview

![Bears WC Product Quick View](http://bearsthemes.com/plugin/images/bears-woocommerce-product-quick-view/bears-wc-product-quick-view.gif "WC product quick view")

## Quick View Modal
[WordPress how to install plugin](https://codex.wordpress.org/Managing_Plugins "WP turorial install plugin")
The modal window shows the product’s:

1. Main featured image
2. Title
3. Price
4. Short description
5. View button
6. Cart buttons

## Compatible
1. WooCommerce 3.x
2. WooCommerce Variation Swatches and Photos
3. WooCommerce Product Bundles

## Changing Template
To customize template, create a folder 'bears-woocommerce-product-quick-view' on your theme:
Example (custom layout quick view button):
```
copy file: plugins/bears-woocommerce-product-quick-view/templates/loop/quick-view-button.php
to your theme: your-theme/bears-woocommerce-product-quick-view/loop/quick-view-button.php
-> edit the file.
```

---

Remove button quick view:
```PHP
add_action('wp_head', 'remove_button_quick_view');
function remove_button_quick_view() {
  global $BWC_Product_Quick_View;
  remove_action('woocommerce_after_shop_loop_item', array($BWC_Product_Quick_View, 'quick_view_button'), 5);
}
```

---

Move button quick view:
```PHP
global $BWC_Product_Quick_View;
add_action('name action ...', array($BWC_Product_Quick_View, 'quick_view_button'), 15);
```

## Hooks
1. add_action('bwc_product_quick_view_before_single_product', ...);
2. add_action('bwc_product_quick_view_after_single_product', ...);
