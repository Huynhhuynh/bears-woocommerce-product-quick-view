<?php
/**
 * Functions used by plugins
 */
if ( ! class_exists( 'BWC_Dependencies' ) )
	require_once 'class-bwc-dependencies.php';

/**
 * WC Detection
 */
if ( ! function_exists( 'is_woocommerce_active' ) ) {
	function is_woocommerce_active() {
		return BWC_Dependencies::woocommerce_active_check();
	}
}

if(! function_exists('bwc_ajaxload_content_product')) {
	/**
	 * @since 1.0.0
	 */
	function bwc_ajaxload_content_product() {
		global $BWC_Product_Quick_View;

		$BWC_Product_Quick_View->quick_view();
		exit();
	}
	add_action( 'wp_ajax_bwc_ajaxload_content_product', 'bwc_ajaxload_content_product' );
	add_action( 'wp_ajax_nopriv_bwc_ajaxload_content_product', 'bwc_ajaxload_content_product' );
}

if(! function_exists('bwc_add_custom_class_product_thumbnail')) {
	/**
	 * @since 1.1.1
	 * 
	 */
	function bwc_add_custom_class_product_thumbnail( $image, $product, $size, $attr, $placeholder ) {
		if( isset( $attr['class'] ) ) {
			$attr['class'] .= ' bwc-product-thumbnail-image';
		} else {
			$attr['class'] = ' bwc-product-thumbnail-image';
		}

		if ( has_post_thumbnail( $product->get_id() ) ) {
			$image = get_the_post_thumbnail( $product->get_id(), $size, $attr );
		} elseif ( ( $parent_id = wp_get_post_parent_id( $product->get_id() ) ) && has_post_thumbnail( $parent_id ) ) { // @phpcs:ignore Squiz.PHP.DisallowMultipleAssignments.Found
			$image = get_the_post_thumbnail( $parent_id, $size, $attr );
		} elseif ( $placeholder ) {
			$image = wc_placeholder_img( $size );
		} else {
			$image = '';
		}

		return $image;
	}
	add_filter( 'woocommerce_product_get_image', 'bwc_add_custom_class_product_thumbnail', 30, 5 );
}
